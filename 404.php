<head><link rel="stylesheet" type="text/css" href="css/homestyle.css"/></head>


<h1>404 – File or Directory not found</h1>
<p>De opgevraagde pagina bestaat niet.</p> 
<p>Dit kan doordat...</p>
<ol class="404ol">
	<li>
		...je als gebruiker stiekem met de queryvars in de URL hebt geknoeid.
	</li>
	<li>
		...er in het menu verwezen wordt naar een artikel dat door de admin net is weggehaald.
	</li >
	<li>
		...programmeerfout in het menu zit.
	</li>
</ol>