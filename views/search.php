<?php
/**
 * Search.php - renders a search page in the main element of the application
 * 
 * @author Bugslayer
 * 
 */
// Include required external scripts
require_once dirname ( __FILE__ ) . '/../components/search.php';

// Check if the user posted a search command
$isSearch = (isset ( $_GET ['search_text'] ));
$searchCmd = "";
if ($isSearch) {
	$searchCmd = $_GET ['search_text'];
}
?>
<form name="search" method="get"
	style="width: 850px; margin-left: auto; margin-right: auto">
	<h1>Zoek</h1>
	<input type="hidden" name="action" value="show" /> <input type="hidden"
		name="page" value="search" />
	<table style="width: 850px">
		<tr>
			<td width="230px"><label for="search_text">Zoekopdracht: </label></td>
			<td width="265px"><input type="text" id="search_text"
				name="search_text" size="50"
				placeholder="Vul hier uw zoekopdracht in"
				value="<?php if ($isSearch) echo $searchCmd ?>"></td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: center"><input type="submit"
				value="Zoek"></td>
		</tr>
	</table>
</form>
<br />
<div id="resultlist">
<?php
// Render the search results only if the user posted a search.
if ($isSearch) {
	$result = search_pattern ( $searchCmd );
	if (sizeof ( $result ) > 0) {
		echo "<ul>";
		foreach ( $result as $row ) {
			echo "<li>";
			echo '<b><a href="?action=show&page=article&id=' . $row ['ID'] . '">' . $row ['Name'] . '</a></b><br/>' . $row ['Content'];
			echo "</li>";
		}
		echo "</ul>";
	} else {
		echo "Geen zoekresultaten gevonden.";
	}
}
?>
</div>
<!-- Include search javascript file -->
<script src="js/search.js"></script>


