<?php 
/**
 * Users.php - renders, well, it should render a list of users
 * 
 * @author Bugslayer
 * 
 */
 
// Check if the request is done by an authorized user. If not, show 401.php and exit
if (!isAuthenticated()) {
	include '401.php';
	exit();
}


?>
<h1>Gebruikers</h1>
<?php

include 'db.php';

$sql = "SELECT Userid, Name_first, Name_middle, Name_last, Email FROM user ORDER BY Name_last ASC";
		$result = $mysqli->query($sql);
		
		if ($result->num_rows > 0) {
		    echo "<table><tr><th>Gebruikers</th><th>naam</th>";
		    // output data of each row
		    while($row = $result->fetch_assoc()) {
		        echo "<tr>
				        <td>De gebruiker ".$row["Userid"]." heet</td>
				        <td>".$row["Name_first"]." ".$row["Name_middle"]." ".$row["Name_last"]." in het echt</td>
				        <td><a href='mailto:".$row["Email"]."'>klik hier om hem/haar te mailen.</A><td>
		        	</tr>";
		    }
		    echo "</table>";
		} else {
		    echo "0 results";
		}
		$mysqli->close();
?>